#include <stdio.h>

#include <pigpio.h>

#define LED 4
int trig = 1;
int echo = 2;
int lecture_echo;
int cm;
double start;

void setup()
{

    gpioSetMode(LED, PI_OUTPUT);
    gpioSetMode(trig, PI_OUTPUT);
    gpioSetMode(echo, PI_INPUT);

    gpioWrite(trig, 0);

    printf("Initialisation");
}

void loop()
{

    gpioWrite(trig, 1);
    gpioDelay(10);
    gpioWrite(trig, 0);
    // lecture_echo = gpioTrigger(echo, 25 , 1) ;
    lecture_echo = gpioSetAlertFunc(echo, 1);
    cm = lecture_echo / 58;
    printf("Distance en cm : + %d \n", lecture_echo);
    gpioDelay(10000);

    start = time_time();

    if ((time_time() - start ) > 10.0)
        {

            start = time_time();
            printf("err \n");
            gpioWrite(LED, 1);
        }



    if (cm < 50)
    {
        gpioWrite(LED, 0);
    }
}

int main(int argc, char *argv[])
{
    

    if (gpioInitialise() < 0)
    {
        fprintf(stderr, "pigpio initialisation failed\n");
        return 1;
    }

    setup();
    while (1)
    {
        loop();
    }

    return 0;
}
