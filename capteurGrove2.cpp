#include "grovepi.h"
using namespace GrovePi;

// sudo g++ -Wall grovepi.cpp grove_button.cpp -o grove_button.out -> without grovepicpp package installed
// sudo g++ -Wall -lgrovepicpp grove_button.cpp -o grove_button.out -> with grovepicpp package installed

int main()
{

    try
    {
        initGrovePi();              // initialize communications w/ GrovePi

        // do this indefinitely
        while (true)
        {
            int button_state = ultrasonicRead(button_pin);      // read the button state
            printf("state =  %d", button_state); // and print it on the terminal screen
           
            delay(1000); // wait 100 ms for the next reading
        }
    }
    catch (I2CError &error)
    {
        printf(error.detail());

        return -1;
    }

    return 0;
}