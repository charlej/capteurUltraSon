#include "grovepi.h"
using namespace GrovePi;

// sudo g++ -Wall grovepi.cpp grovepi_analog_read.cpp -o grovepi_analog_read.out -> without grovepicpp package installed
// sudo g++ -Wall -lgrovepicpp grovepi_analog_read.cpp -o grovepi_analog_read.out -> with grovepicpp package installed

int main()
{
	int ultrasonic_ranger = 3; // we use port A0
	printf("int ultrasonic");

	try
	{
		printf("try \n");
		initGrovePi(); //initialize communication w/ GrovePi
		printf("initgrovepi \n");
		initDevice(ultrasonic_ranger);
		setMaxI2CRetries(10000);
		printf("initDevice \n");
		// set the pin mode for reading
		pinMode(ultrasonic_ranger, INPUT);
		printf("pinMode INPUT \n");
		// continuously read data
		while (true)
		{
			printf("Distance %d \n", ultrasonicRead(ultrasonic_ranger));

			delay(1000);
		}
	}

	catch (I2CError &error)
	{
		printf("I2C Erreur \n");
		printf(error.detail());

		return -1;
	}

	return 0;
}
